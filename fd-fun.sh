#!/usr/bin/env	bash

# vim: ft=bash

# You _can_ do a redirection without any command...
>&2

# ...but this has no effect on downstream lines:
echo dummy # try fd-fun.sh 2> err: 'dummy' still goes to the terminal.

# If for some reason you really want to redirect all standard output to stderr,
# do
exec >&2
echo "now this goes to &2" # fd-fun.sh 2> err && cat err

# Reset output to stdout...
exec >&1

# If you attempt to redirect to a fd that is NOT open for writing, you get an
# error:
>&3 # Bad file descriptor

# If you do open f3 for writing (3>file, 3>&1, 3>/dev/null ...), the error goes
# away:
echo "this goes to fd 3" >&3 # try ./fd-fun 3> fd3; cat fd3

# to _test_ if a fd is open for writing, you _could_ do

if >&3; then
	echo "fd 3 open for writing"
else
	echo "fd 3 closed for writing"
fi

# ... but this will also give you a Bad file descriptor error. Perhaps we can
# just discard STDERR?

if >&3 2>/dev/null; then
	echo "fd 3 open for writing"
else
	echo "fd 3 closed for writing"
fi

# Ah, but by the time we redirect fd 2, we have already attempted to write to fd
# 3, so the error message gets printed. So let's try reversing them:

if 2>/dev/null >&3;  then
	echo "fd 3 open for writing"
else
	echo "fd 3 closed for writing"
fi

# How about _closing_ fd 2?

if >&2- >&3;  then
	echo "fd 3 open for writing"
else
	echo "fd 3 closed for writing"
fi

# One thing that puzzles me is that closing fd2 above does not cause an error
# when the shell writes to it, as it does when we try to echo to fd3. However,
# if I close fd3 (assuming it's opened, e.g. by doing ./fd-fun.sh 3>&1), then
# writing to fd3 _does_ cause a Bad file descriptor:

exec >&3-
printf "This goes to fd3, 'xcept it's closed.\n" >&3

exec >&2- # closes fd2 (stderr)
>&4       # no error, even though it "Bad file desciptor..." to fd2

# There is more to this, see e.g.  https://unix.stackexchange.com/questions/206786/testing-if-a-file-descriptor-is-valid
