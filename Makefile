
# I'm passing --number-sections here because setting number-sections: true in the
# metadata doesn't seem to work.

# bash_prog_ref.pdf: bash_prog_ref.md metadata.yaml Makefile
# 	pandoc --standalone \
# 		--to=latex \
# 		--metadata-file=metadata.yaml \
# 		--output $@ $<

bash_prog_ref.pdf: bash_prog_ref.tex
	pdflatex $<

