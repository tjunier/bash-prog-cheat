# examples of function definitions
# source this file to check them. 

# "long" syntax: 'function' keyword, parens, braces.
function f1 () { ls -l; }

# note the spaces around the braces: the following
# f2() {ls -l;}  # WRONG!
# is parsed as f1, (), {ls, -l;}
# instead of   f2, (), {, ls, -l, }

# You can leave out the 'function' keyword...
f2() { ld -l; }

# ...or the parens
function f3 { ls -l; }

# but not both
# f4 { ls -l; }  # WRONG!

# The body is a _compound command_. Recall that this may be:
# i) a looping construct

f5() for a in {1..5}; do echo $a; done

# (Note that the { } are not needed, as the for...done is in itself a compound
# command)
# ii) a conditional construct

f6() if true; then echo true; fi

# (again, { } not required)
# iii) a grouping construct, which is either a list between parens...

f7() (ls | wc) # spaces optional <- ()| are metacharacters

# ... or a terminated list between braces:

f8() { ls -l; } # here the terminator is ;

# the list terminator can also be a newline...

f9() {
    ls -l
}

#... or an &

f10() { sleep 10 & }

# NOTE: compound commands all start and end with a reserved word (if, case, for,
# while, until, { ; fi, esac, done, } ) or control operator ( (, ) ). This is why
# the braces are not necessary around conditionals, loops, or subshell
# groupings.

# NOTE: the manual says: "If the function reserved word is used, but the
# parentheses are not supplied, the braces are required"
# (https://www.gnu.org/software/bash/manual/html_node/Shell-Functions.html#Shell-Functions),
# but the following seems to work:

function f11 if true; then echo true; fi
