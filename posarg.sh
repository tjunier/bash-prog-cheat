#!/bin/bash

setx() {
	if [[ -v xtrace ]]; then
		set -x
	fi
}

# exemplify behaviour of $* and $@, within or without double quotes.

if [[ '-x' = "$1" ]]; then
	xtrace=1
	shift
fi

printf "\n${#} args\n"

printf "\n" 

printf "split+glob context\n"
printf -- "------------------\n"

printf "\n \$* : separate words, each word itself split+globbed\n";        setx; printf "<%s> " $*; echo;   set +x
printf "\n\"\$*\": single word, joined by IFS[0]\n";                       setx; printf "<%s> " "$*"; echo; set +x
printf "\n \$@ : separate words, each word itself split+globbed\n";         setx; printf "<%s> " $@; echo;   set +x
printf "\n\"\$@\": separate words, no split\n";                            setx; printf "<%s> " "$@"; echo; set +x
printf "\n\"abc\$@def\": separate words, no split, bounded by abc, def\n"; setx; printf "<%s> " "abc$@def"; echo; set +x

printf "\n" 

printf "not split+glob context\n"
printf -- "----------------------\n"

printf "\n \$* : separate words, no split+glob\n"
var=$*; IFS=: printf "var=\$* -> var: <%s>\n" "$var"
printf "\n \$@  : single word, space-separated, no split+glob\n"
var=$@; printf "var=\$@ -> var: <%s>\n" "$var"
var="$@"; printf "var=\"\$@\" -> var: <%s>\n" "$var"
