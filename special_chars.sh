#!/bin/bash

# Special characters

# If quoting a character (say, with a \) changes anything, then the character
# is special. Conversely, if a character isn't special, then quoting it has no
# effect, e,g:

echo *
echo \* # different -> * is special
ech\o * # same as echo -> o is not special

# whitespace: space, tab, newline
name=Marcus Tullius Cicero
name=Marcus\ Tullius\ Cicero

# other (non-whitespace) metacharacters: & | ( ) < >
echo &
echo \&
echo dog | cat
echo dog \| cat
echo foo > foo.txt
echo foo \> foo.txt
(ls)
\(ls)
(ls\)

# assignment
a=1
a\=1

# expansions
echo $name
echo \$name

# conditionals
[[ true ]] && echo true
\[[ true ]] && echo true
[[ true \]] && echo true

# this works, however:
\[ true ] && echo true

# glob characters: * ?
echo *
echo \*

# comment
ls # lists files
ls \# lists files

# semicolon
if [ -d . ]; then echo true; fi
if [ -d . ]\; then echo true; fi

# quotes and escape
echo "my name is "Bond""
echo "my name is \"Bond\""
# strong quotes _can_ be escaped
echo 'atha   
echo \'atha
echo \
echo \\



