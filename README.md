README
======

This document contains what I found I frequently need to look up when _programming_ (as opposed to working interactively) in Bash.

It can be used as a companion to the Bash scripting course.
