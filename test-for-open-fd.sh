#!/usr/bin/env bash

# vim: ft=bash

echo "Testing whether fd 3 is open for writing"

if  { >&3; } 2>&-; then
	echo "fd 3 open for writing"
else
	echo "fd 3 closed for writing"
fi
